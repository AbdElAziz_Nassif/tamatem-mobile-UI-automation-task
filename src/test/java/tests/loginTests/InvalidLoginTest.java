package tests.loginTests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.PhoneLoginPage;
import tests.BaseTest;

import static filesReaders.ReadFromFiles.getJsonStringValueByKey;
import static pages.PhoneLoginPage.PHONE_NUMBER_ERR_MESSAGE;

public class InvalidLoginTest extends BaseTest {

    @Test(description = "Check that user will get error message after entering invalid phone number")
    public void testInvalidLogin(){
        PhoneLoginPage phoneLoginPage = splashScreenPage.clickSignInWithPhoneBtn();
        phoneLoginPage = phoneLoginPage.enterPhoneNumberToLogin(getJsonStringValueByKey("RegistrationTestData.json", "invalidPhoneNumber"));
        Assert.assertTrue(phoneLoginPage.isPhoneNumberErrorMessageDisplayed(),
                "Phone number error message should be displayed");
        Assert.assertEquals(phoneLoginPage.getPhoneNumberErrorMessage(), PHONE_NUMBER_ERR_MESSAGE,
                "Phone number error message text content is not as expected");
    }

}
