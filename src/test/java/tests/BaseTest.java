package tests;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import pages.SplashScreenPage;

import java.net.MalformedURLException;
import java.net.URL;

import static filesReaders.ReadFromFiles.getPropertyByKey;

public class BaseTest {
    protected SplashScreenPage splashScreenPage;
    protected AndroidDriver<AndroidElement> driver;
    private final static String APPIUM_CAPS_PROPERTY_FILE = "appiumCaps.properties" ;

    @BeforeClass
    public void setupDriverAndCapabilities() throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("automationName", getPropertyByKey(APPIUM_CAPS_PROPERTY_FILE, "AUTOMATION_NAME"));
        caps.setCapability("platformName", getPropertyByKey(APPIUM_CAPS_PROPERTY_FILE, "PLATFORM_NAME"));
        caps.setCapability("platformVersion", getPropertyByKey(APPIUM_CAPS_PROPERTY_FILE, "PLATFORM_VERSION"));
        caps.setCapability("deviceName", getPropertyByKey(APPIUM_CAPS_PROPERTY_FILE, "DEVICE_NAME"));
        caps.setCapability("app", System.getProperty("user.dir") + getPropertyByKey(APPIUM_CAPS_PROPERTY_FILE, "APP_PATH"));

        driver = new AndroidDriver<AndroidElement>(new URL(getPropertyByKey(APPIUM_CAPS_PROPERTY_FILE, "APPIUM_URL")), caps);
        splashScreenPage = new SplashScreenPage(driver);
    }


    @AfterClass
    public void quitDriver() {
        driver.quit();
    }
}
