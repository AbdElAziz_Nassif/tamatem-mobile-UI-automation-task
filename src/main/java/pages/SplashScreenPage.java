package pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

public class SplashScreenPage extends BasePage{

    By signInWithPhoneBtn = By.xpath("//android.widget.Button[@text='Sign in with Phone']") ;
    By enterAsGuestBtn = By.xpath("//android.widget.Button[@text='Enter as a Guest']") ;

    public SplashScreenPage(AndroidDriver driver) {
        super(driver);
    }

    public PhoneLoginPage clickSignInWithPhoneBtn ()
    {
         clickElement(signInWithPhoneBtn);
         return new PhoneLoginPage(driver);
    }
    public HomePage clickEnterAsGuestBtn ()
    {
        clickElement(enterAsGuestBtn);
        return new HomePage(driver);
    }
}
