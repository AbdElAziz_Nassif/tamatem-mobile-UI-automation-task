package pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;
import java.time.Duration;

public class BasePage {

    AndroidDriver<AndroidElement> driver;
    WebDriverWait wait;

    public BasePage(AndroidDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);
    }

    protected AndroidElement locateElement(By elementLocator) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
        wait.until(ExpectedConditions.elementToBeClickable(elementLocator));
        return driver.findElement(elementLocator);
    }

    protected void typeOnInputField(By elementLocator, String text) {
        locateElement(elementLocator).sendKeys(text);
    }

    protected void clickElement(By elementLocator) {
        locateElement(elementLocator).click();
    }

    protected String getTextOfElement(By elementLocator) {
        return locateElement(elementLocator).getText();
    }
    protected boolean isElementDisplayed(By elementLocator) {
        return locateElement(elementLocator).isDisplayed();
    }

}
