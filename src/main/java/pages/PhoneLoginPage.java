package pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

public class PhoneLoginPage extends BasePage{

    public PhoneLoginPage(AndroidDriver driver) {
        super(driver);
    }
    By phoneField = By.xpath("//android.widget.EditText[@resource-id='phone-number']") ;
    By continueBtn = By.xpath("//android.widget.Button[@text='Continue']") ;
    public static final String PHONE_NUMBER_ERR_MESSAGE = "The entered phone number is wrong, please make sure that you enter your country code in front of the phone number!" ;
    String phoneNumberErrorLocator = "//android.widget.TextView[@text='%s']" ;
    By errorMessage = By.xpath(String.format(phoneNumberErrorLocator, PHONE_NUMBER_ERR_MESSAGE)) ;

    public PhoneLoginPage enterPhoneNumberToLogin (String phoneNumber)
    {
        typeOnInputField(phoneField,phoneNumber);
        clickElement(continueBtn);
        return this ;
    }
    public String getPhoneNumberErrorMessage ()
    {
        return getTextOfElement(errorMessage) ;
    }
    public boolean isPhoneNumberErrorMessageDisplayed ()
    {
        return isElementDisplayed(errorMessage) ;
    }

}
