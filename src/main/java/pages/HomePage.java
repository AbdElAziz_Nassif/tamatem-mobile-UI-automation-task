package pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

public class HomePage extends BasePage {


    public HomePage(AndroidDriver driver) {
        super(driver);
    }

    By chatIcon = By.xpath("//android.view.View[@text='Chat']");

    public ChatPage clickChatIon ()
    {
        clickElement(chatIcon);
        return new ChatPage (driver);
    }

}
