package pages;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

public class ChatPage extends BasePage {

    public ChatPage(AndroidDriver driver) {
        super(driver);
    }

    By newTopicBtn = By.xpath("//android.widget.Button[@text='plus-icon-1 New topic']");
    By sendMessageIcon = MobileBy.xpath("//android.widget.Button[@text='global-chat-icon Chat']");
    By messageInput = By.xpath("//android.widget.EditText");
    public static final String INVALID_CHAT_MSG_TEXT = "You need to be at least level 5 if you want to chat! Play a few games to reach it!";
    String getInvalidChatMsgLocator = "//android.view.View[@text='%s']" ;
    By invalidChatErrorMessage = By.xpath(String.format(getInvalidChatMsgLocator, INVALID_CHAT_MSG_TEXT));

    public ChatPage clickAddNewTopic() {
        clickElement(newTopicBtn);
        return this;
    }

    public ChatPage typeMessage(String messageText) {
        typeOnInputField(messageInput, messageText);
        return this;
    }

    public ChatPage clickSendMessage ()
    {
        clickElement(sendMessageIcon);
        return this;
    }

    public boolean isInvalidChatErrMessageDisplayed() {
        return isElementDisplayed(invalidChatErrorMessage);
    }

    public String getInvalidChatErrMessageText() {
        return getTextOfElement(invalidChatErrorMessage);
    }

    public boolean isChatTextBoxDisplayed()
    {
        return isElementDisplayed(messageInput) ;
    }
}
